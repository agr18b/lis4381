> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Apps

## Andrew Reyes

### Project 1 Requirements:

1. Create a luancher icon image and display it in both interfaces
2. Add bakcground color to both activites
3. add border image around image and button
4. Add text shadow to button text

#### README.md file should include the following items:

* Screenshot of first interface
* Screenshot of second interface

#### Assignment Screenshots:

*Screenshot of the first and second interface

![First Interface](img/1.PNG) ![Second Interface](img/2.PNG)

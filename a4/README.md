> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Apps

## Andrew Reyes

### Assignment 4 Requirements:

1. Create localhost website
2. Edit PHP files located in each folder to each segment
3. Change navigation and header links

#### README.md file should include the following items:

* Screenshot of Carousel
* Screenshot of Data validation all correct
* Screenshot of data validation all incorrect

#### Assignment Screenshots:

*Screenshot of carousel

![First Interface](img/1.PNG) 

*Screenshot of correct data validation

![ER Model](img/2.png)

*Screenshot of incorrect data validation

![Incorrect Data](img/3.PNG)

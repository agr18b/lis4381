> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Apps

## Andrew Reyes

### Assignment 2 Requirements:

1. Create a mobile recipe app using Android Studio
2. Provide screenshots for both the main page and recipe page
3. Andswer Chapter 3 and 4 Questions

#### README.md file should include the following items:

* Course title, your name, Assignment number, Assignment requirements
* Screenshot of HealthyRecipes main page
* Screenshot of HealthyRecipes recipe and instructions page

#### Assignment Screenshots:

*Screenshot of HealthyRecipes main page

![Main page screenshot](img/main.PNG)

*Screenshot of HealthyEcipes recipe and instructions page

![Recipe and instrustions page screenshot](img/recipe.PNG)

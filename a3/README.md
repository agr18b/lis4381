> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Apps

## Andrew Reyes

### Assignment 3 Requirements:

1. Create a Ticket Pricing app using Android Studio
2. Provide screenshots of the working program
3. Create MySQL Pet Store database

#### README.md file should include the following items:

* Screenshot of ER Model
* Screenshot of Android App Running both interfaces
* Links to following files: a3.mwb and a3.sql

#### Assignment Screenshots:

*Screenshot of the first and second interface

![First Interface](img/img1.PNG) ![Second Interface](img/img2.PNG)

*Screenshot of the ER Model

![ER Model](img/img3.PNG)

[a3.mwb](doc/Assignment3SQL.mwb)

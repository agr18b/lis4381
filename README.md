> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381 Mobile Web Apps

## Andrew Reyes

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Install AMPPS
	- Install JDK
	- Install Android studio and creat My First App
	- Provide screenshots of installations
	- Create Bitbucket repo
	- Complete Bitbucket tutorials
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create mobile recipe app
	- Insert images, text, and lists
	- Chapter 3 and 4 Questions
	
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create mobile Ticket Pricing app
	- Create PetStore database using MySQL
	
4. [A4 README.md](a4/README.md "My A5 README.md file")
    - Create localhost website for all assignments
	- Edit php files appropriatly
	- Have user input to the data base

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create server-side validation
	- Create a chart consisting of all the data from the database

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a mobile app inside andoird studio that displays itself as a business card
	- Uses two interfaces

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - TBD
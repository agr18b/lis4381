import java.util.Scanner;

//import jdk.internal.jshell.tool.resources.l10n;

import java.util.Random;

class q5
{
    public static void main(String[] args)
        
        System.out.println("Program prompts user to enter desired number of pseudoranodom-generated integers");


        System.out.println();
        System.out.println();

        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        int arraySize = 0;
        int i = 0;

        System.out.print("Enter desired number of pseudorandom-generated integers (min 1): ");
        arraySize= sc.nextInt();
        
        int myArray[] = new int[arraySize];

        System.out.println("for loop:");
        for(i=0; i< myArray.length; i++)
        {
            System.out.println(r.nextInt());
        }
        
        System.out.println("\nEnhanced for loop:");
        for (int n: myArray)
        {
            System.out.println(r.nextInt());
        }
        

        
    }
}
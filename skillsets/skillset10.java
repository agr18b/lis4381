import java.util.Scanner;
import java.util.*;

class skillset10
{
    public static void main(String args[])
    {
        System.out.println();
        System.out.println("Program populates ArrayList of strings with user-entered animal type values.");
        System.out.println("Examples: Polar bear, Guinea pig, dog, cat, bird");
        System.out.println("Program continues to collect user-entered values until user types n");
        System.out.println("Prgoram displays ArrayList values after each iteration, as well as size.");

        System.out.println();

        Scanner sc = new Scanner (System.in);
        ArrayList<String> obj = new ArrayList<String>();
        String cont = "y";
        String myStr = "";

        int num = 0;

        while (cont.equals("y"))
            {
                System.out.print("Enter animal type: ");
                myStr = sc.nextLine();
                obj.add(myStr);
                num = obj.size();
                
                System.out.println("ArrayList elements :" + obj);
                System.out.println("ArrayList Size = " + num);

                System.out.println("Continue? y or n: ");
                cont = sc.next();
                sc.nextLine();
                System.out.println();
            }
        
    }
}


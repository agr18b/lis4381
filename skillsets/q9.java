import java.util.Scanner;

class q9
{
    public static void main(String args[])
    {

        System.out.println("1. Program creates array size at run-time.");
        System.out.println("2. Program displays array size.");
        System.out.println("3. Program rounds sum and average of numebrs to two decimal places.");
        System.out.println("4. Numbers must be flaot data type, not double");

        System.out.println();
        Scanner sc = new Scanner (System.in);
        int arraySize = 0;
        float sum = 0.0f;
        float average = 0.0F;

        System.out.println("Enter array size:");
        arraySize = sc.nextInt();
        System.out.println();

        float numsArray[] = new float[arraySize];
        for(int i = 0; i < arraySize ; i++)
        {
            System.out.print("Enter num " + (i + 1 ) + ": ");
            numsArray[i] = sc.nextFloat();
            sum = sum + numsArray[i];
        }
        System.out.println();

        System.out.println("Sum: " + String.format("%.2f", sum));
        average = sum / arraySize;
        System.out.println("Average: " + String.format ("%.2f", average));
        System.out.println();
    }
}
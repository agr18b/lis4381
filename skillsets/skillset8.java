import java.util.Scanner;

public class skillset8
{
    public static void main(String[]args)
    {

        System.out.println("\nProgram evaluates largest of three integers.");
        System.out.println("\nNote: Program checks for integers and non-numeric values.");

        int num1 = 0;
        int num2 = 0;
        int num3 = 0;

        Scanner input = new Scanner(System.in);

        System.out.print("\nPlease enter first number: ");
        while (!input.hasNextInt())
        {
            System.out.println("Not a valid integer!\n");
            input.next();
            System.out.print("Please try again. Enter first number: ");
        }
        num1 = input.nextInt();

        System.out.print("\nPlease enter second number: ");
        while (!input.hasNextInt())
        {
            System.out.println("Not a valid integer!\n");
            input.next();
            System.out.print("Please try again. Enter second number: ");
        }
        num2 = input.nextInt();

        System.out.print("\nPlease enter third number: ");
        while (!input.hasNextInt())
        {
            System.out.println("Not a valid integer!\n");
            input.next();
            System.out.print("Please try again. Enter third number: ");
        }
        num3 = input.nextInt();

        if (num1 > num2 && num1 > num3)
            System.out.println("First number is the largest");
        else if (num2 > num1 && num2 > num3)
            System.out.println("Second number is largest");
        else if (num3 > num1 && num3 > num2)
            System.out.println("Third number is largest");
        else
            System.out.println("Integers are equal");
    }

}
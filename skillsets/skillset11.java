import java.util.Scanner;
import java.util.*;
class skillset11
{
    public static void main(String args[])
    {
        System.out.println();
        System.out.println("Program searches user-entered integer w/in array of integers.");
        System.out.println("Create an array with the following values: 3, 2, 4, 99, -1, -5, 3, 7");
        
        Scanner sc = new Scanner (System.in);
        int num = 0;
        int userNum = 0;
        int arrayPos = 0;
        int[] intArray = new int[]{ 3, 2, 4, 99, -1, -5, -5, 3, 7};

        System.out.println();
        System.out.println("Array Length: " + intArray.length);
        System.out.print("Enter Search Value: ");
        userNum = sc.nextInt();

        System.out.println();

        while (num < 9)
        {
            if (intArray[arrayPos] == userNum)
            {
                System.out.println( userNum + " found at index " + arrayPos);
            }
            else
            {
                System.out.println( userNum + " *not* found at index " + arrayPos);
            }
            num ++;
            arrayPos ++;
        }
    }
}
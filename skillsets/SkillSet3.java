class SkillSet3
{
    public static void main(String args[])
    {
        System.out.println();
        System.out.println("Program loops through array of strings.");
        System.out.println("Use following values: dog, cat, bird, fish, insect.");
        System.out.println("Use Following loop structures: for, enhanced for, while. do...while.");
        System.out.println();
        System.out.println("Pretest loops: for, enhanced for, while. Posttest loop: do...while.");
        System.out.println();


        String[] animal = new String[] {"dog", "cat", "bird", "fish", "insect"};
        
        //for loop
        System.out.println("for loop:");
        for (String i : animal)
        {
            System.out.println(i);
        }
        System.out.println();

        //enhanced for loop
        System.out.println("enhanced for loop:");
        for (String item: animal)
        {
            System.out.println(item);
        }
        System.out.println();
        
        //while loop
        int index = 0;
        System.out.println("while loop:");
        while (index < animal.length)
        {
            System.out.println(animal[index]);
            index++;
        }
        System.out.println();

        //do while loop
        System.out.println("do while loop:");
        int n = 0;
        do
        {
            System.out.println(animal[n]);
            n++;
        }
        while (n < 5);
    }
}

import java.util.Scanner;

class SkillSet2
{
    public static void main(String args[])
    {
        System.out.println("Program Evaluates largest of two integers.");
        System.out.println("Note: Program does *not* check for non-numeric characters.");
        System.out.println();

        int num1;
        int num2;

        Scanner sc =
         new Scanner(System.in);

        System.out.print("Enter first integer: ");
        num1 = sc.nextInt();
        System.out.print("Enter second integer: ");
        num2 = sc.nextInt();

        System.out.println();
        if (num1 > num2)
            System.out.println(num1 + " is larger than " + num2);
        else if (num2 > num1)
            System.out.println(num2 + " is larger than " + num1);
        else
            System.out.println("Integers are equal");    
        
    }
}
import java.util.Scanner;
import java.util.*;

class skillset12
{
    public static void main(String args[])
    {
        System.out.println("Sphere Volume Program");
        System.out.println("Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches, and rounds to two decimal places");
        System.out.println("Must use Java's *built-in* PI and pow() capabilites");
        System.out.println("Program checks for non-integers and non-numeric values.");
        System.out.println("Program continues to prompt for user entry until no longer requested, prompt accepts upper or lower case letters.");

        Scanner sc = new Scanner (System.in);

        double temperature = 0.0;
        char choice = ' ';
        char type = ' ';

        do
        {
            System.out.print("\nFahrenheit to Celsius? type \"f\", or Celsius to Fahrenheit? Type \"c\": ");
            type = sc.next().charAt(0);
            type = Character.toLowerCase(type);

            if(type == 'f')
            {
                System.out.print("\nEnter temperature in Fahrenheit: ");
                temperature = sc.nextDouble();
                temperature = ((temperature - 32) * 5)/9;
                System.out.println("Temperature in Celsius = " + temperature);
            }
            else if(type == 'c')
            {
                System.out.print("\nEnter temperature in Celsius: ");
                temperature = sc.nextDouble();
                temperature = (temperature * 9/5) + 32;
                System.out.println("Temperature in Fahrenheit = " + temperature);
            }
            else
            {
                System.out.println("INcorrect entry. Please try again.");
            }

            System.out.print("\nDo you want to convert a temperature (y or n)? ");
            choice = sc.next().charAt(0);
            choice = Character.toLowerCase(choice);
        }
        while (choice == 'y');

        System.out.println("\nThank you for using our Temperature Conversion Program!");

    }
}
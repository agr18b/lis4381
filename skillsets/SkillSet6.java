import java.util.Scanner;

class SkillSet6
{
    public static void displayProgram()
    {
        System.out.println("Program prompts user for first name and age, then prints results.");
        System.out.println("1) displayProgram(): Void method that displays program requirements.");
        System.out.println("2) myVoidMethod():");
        System.out.println("\ta. Accepts two arguments: String and int.");
        System.out.println("\tb. Prints user's frist name and age.");
        System.out.println("3) Accepts two arguments: String and int.");
        System.out.println("\ta. Accepts two arguments: String and int.");
        System.out.println("\tb. Returns String containing first name and age."); 
    }
    public static void myVoidMethod(String first, int age)
    {
        System.out.println(first + " is " + age);
    }

    public static String myValueReturningMethod(String first, int age)
    {
        return first + " is " + age;
    }

    public static void main(String args[])
    {
        displayProgram();

        String firstName="";
        int userAge = 0;
        String myStr = "";
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter first name: ");
        firstName=sc.next();

        System.out.print("Enter age: ");
        userAge = sc.nextInt();

        System.out.println();

        System.out.print("Void method call: ");
        myVoidMethod(firstName, userAge);

        System.out.print("value-returning method call: ");
        myStr = myValueReturningMethod(firstName, userAge);
        System.out.println(myStr);
    }
}
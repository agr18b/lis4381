import java.util.Scanner;

public class DecisionStructures
{
    public static void main(String[] args)
    {
        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use the following characters: W or w, C or c, H or h, N or n");
        System.out.println();

        String myStr = "";
        char myChar = ' ';
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter phone type: ");
        myStr = sc.next().toLowerCase();
        myChar = myStr.charAt(0);

        System.out.println();
        System.out.prtintln("If...Else Statements: ");

        System.out.println("...else:");

        if (myChar == 'w')
        {
            System.out.println("phone type: work");
        }
        else if (myChar == 'c')
        {
            System.out.println("phone type: cell");
        }
        else if (myChar == 'h')
        {
            System.out.println("phone type: home");
        }
        else if (myChar == 'n')
        {
            System.out.println("phone type: none");
        }
        else
        {
            System.out.println("Incorrect character entry.")
        }
        
        System.out.println("Switch Statements:");

        switch(myChar)
        {
            case 'w':
                System.out.println("Phone type: work");
                break;
            case 'c':
                System.out.println("Phone type: cell");
                break;
            case 'h':
                System.out.println("Phone type: home");
                break;
            case 'n':
                System.out.println("Phone type: none");
                break;
            default: 
                System.out.println("Incorrect character entry.");
                break;
        }
    }
}
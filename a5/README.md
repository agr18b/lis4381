> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Apps

## Andrew Reyes

### Assignment 5 Requirements:

1. Develop serverside validation of a form in php
2. Develop a form that adds data into a database
3. Chapter questions (10, 11, 12, 19)

#### README.md file should include the following items:

* Screenshot of index.php for the data in petstore
* Screenshot of server-side validation

#### Assignment Screenshots:

*Screenshot of index.php

![index.php](img/1.PNG) 

*Screenshot of serve-side validation

![server-side validation](img/2.png)

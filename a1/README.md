> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Apps

## Andrew Reyes

### Assignment 1 Requirements:

1. Ampps Installation
2. Java Installation
3. Android Studio Installation
4. Creat bitbucket repo
5. Chapter 1 & 2 Questions

#### README.md file should include the following items:

* Screenshot of ampps installation
* Screenshot of Java installation
* Screenshot of Android studio first app

>
> #### Git commands w/short descriptions:

1. git init: creates a new git repository
2. git status: displays the state of the working directory and the staging area
3. git add: adds all modified files to the current directory
4. git commit: puts your changes into your local repo
5. git push: used to upload to local repository content to a remote repository.
6. git pull: used to fetch and download content from a remote repository to match that content
7. git config: a convenience function that is used to set git configuration values on a global or local project level. 

#### Assignment Screenshots:

*Screenshot of running ampps

![Running Ampps screenshot](img/amps.PNG)

*Screenshot of hello world

![Running Java screenshot](img/JavaHelloWorld.PNG)

*Screenshot of Android Studio

![Running Android Studio screenshot](img/Android.PNG)

#### Tutorials

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://confluence.atlassian.com/bitbucket/create-a-git-repository-759857290.html/ "Bitbucket Station Locations")

